# SparkFun USB-C PD Test Demo 

This repository contains a demo functional test for the 
[SparkFun USB-C Power Delivery Board](https://www.sparkfun.com/products/15801). 

The functional test is implemented using a 
[Binho Nova USB Host Adapter](https://binho.io/?utm_source=fixturfab&utm_medium=web&utm_campaign=fixturfab_artemis)
to configure the Power Delivery Board and verify that the correct voltages are
output. 

## SparkFun Power Delivery Board Overview 
The SparkFun Power Delivery Board uses a USB-C connector to provide an output 
voltage of 5-20V and up to 100W of power. The board uses a standalone 
controller to negotiate with a power adapter and have it switch to a voltage
higher than 5V. 

![SparkFun USB-C Power Delivery Board](https://cdn.sparkfun.com//assets/parts/1/4/4/1/4/15801-SparkFun_Power_Delivery_Board_-_USB-C__Qwiic_-01.jpg)

A [STMicroelectronics STUSB4500](https://www.st.com/en/interfaces-and-transceivers/stusb4500.html) 
is used as the standalone controller to negotiate power. This device implements 
a proprietary algorithm to negotiate a power delivery contract with the power
adapter. To supply a power other than 5V, the STUSB4500 must be configured over
I2C.

## Binho Nova Overview

## Test Setup 

Clone the repository 

```bash
git clone https://gitlab.com/fixturfab-example-projects/sparkfun-usbc-pd-test-demo.git
```

Install the required dependencies
```bash
cd sparkfun-usbc-pd-test-demo
pip install -r requirements.txt
```

### Hardware Setup and Device Permissions

If this is the first time a Binho Nova has been used on your system, you will 
need to configure your system. Follow the [Binho Nova Hardware Setup guide](https://support.binho.io/getting-started/hardware-setup)

## Running the Tests

To run the tests, simply run the `pytest` command within the 
`sparkfun-usbc-pd-test-demo` directory using a terminal: 

```bash
pytest -x
```

The tests will then run, and produce the following output: 

```bash 
~/PycharmProjects/sparkfun-usbc-pd-test-demo# pytest
======================================= test session starts ===========================================================
platform linux -- Python 3.8.2, pytest-5.4.3, py-1.9.0, pluggy-0.13.1
rootdir: ~/PycharmProjects/sparkfun-usbc-pd-test-demo
collected 21 items                                                                                                                                                                                                                                                                                            

test_sparkfun_usbc_pd/test_config_stusb4500.py .....................                                                                                                                                                                                                                                    [100%]

======================================= 21 passed in 6.65s ============================================================
```

The `-x` or `--exitfirst` option stops the execution of the tests instantly on 
the first failure.