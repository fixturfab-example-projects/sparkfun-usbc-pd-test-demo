import pytest


class TestDevice:
    """
    Tests for detecting a device and verifying that it can be communicated with
    """
    def test_stusb4500_exists(self, test_fixture):
        """
        Verify that a STUSB4500 device with address 0x28 can be found
        """
        expected_address = 0x50
        assert test_fixture.host_adapter.scanAddrI2C(0, expected_address) == "-I2C0 SCAN 0x50 OK"


class TestConfig:
    """
    Test STUSB4500 Configuration

    A configuration is written to the attached STUSB4500 device. The devices
    configuration is then validated.
    """
    @pytest.fixture(autouse=True, scope='class')
    def _prepare(self, test_fixture):
        """
        Configure the STUSB4500 prior to validating it's settings
        :param test_fixture:
        :return:
        """
        test_fixture.write(default_values=True)
        test_fixture.read()

        test_fixture.set_voltage(1, 5)
        test_fixture.set_current(1, 1.5)
        test_fixture.set_lower_voltage_limit(1, 5)
        test_fixture.set_upper_voltage_limit(1, 5)

        test_fixture.set_voltage(2, 20)
        test_fixture.set_current(2, 5)
        test_fixture.set_lower_voltage_limit(2, 5)
        test_fixture.set_upper_voltage_limit(2, 10)

        test_fixture.set_voltage(3, 12)
        test_fixture.set_current(3, 5)
        test_fixture.set_lower_voltage_limit(3, 20)
        test_fixture.set_upper_voltage_limit(3, 10)

        test_fixture.set_flex_current(2)
        test_fixture.set_usb_comm_capable(0)
        test_fixture.set_gpio_ctrl(3)

        test_fixture.write()

    def test_pdo1_voltage(self, test_fixture):
        assert test_fixture.get_voltage(1) == 5

    def test_pdo1_current(self, test_fixture):
        assert test_fixture.get_current(1) == 1.5

    def test_pdo2_voltage(self, test_fixture):
        assert test_fixture.get_voltage(2) == 20

    def test_pdo2_current(self, test_fixture):
        assert test_fixture.get_current(2) == 5.0

    def test_pdo3_voltage(self, test_fixture):
        assert test_fixture.get_voltage(3) == 12

    def test_pdo3_current(self, test_fixture):
        assert test_fixture.get_current(3) == 5

    def test_pdo1_lower_voltage_limit(self, test_fixture):
        assert test_fixture.get_lower_voltage_limit(1) == 0

    def test_pdo2_lower_voltage_limit(self, test_fixture):
        assert test_fixture.get_lower_voltage_limit(2) == 5

    def test_pdo3_lower_voltage_limit(self, test_fixture):
        assert test_fixture.get_lower_voltage_limit(3) == 20

    def test_pdo1_upper_voltage_limit(self, test_fixture):
        assert test_fixture.get_upper_voltage_limit(1) == 5

    def test_pdo2_upper_voltage_limit(self, test_fixture):
        assert test_fixture.get_upper_voltage_limit(2) == 10

    def test_pdo3_upper_voltage_limit(self, test_fixture):
        assert test_fixture.get_upper_voltage_limit(3) == 10

    def test_get_flex_current(self, test_fixture):
        assert test_fixture.get_flex_current() == 2.0

    def test_get_pdo_number(self, test_fixture):
        assert test_fixture.get_pdo_number() == 2

    def test_get_external_power(self, test_fixture):
        assert test_fixture.get_external_power() == 0

    def test_usb_comm_capable(self, test_fixture):
        assert test_fixture.get_usb_comm_capable() == 0

    def test_config_ok_gpio(self, test_fixture):
        assert test_fixture.get_config_ok_gpio() == 2

    def test_gpio_ctrl(self, test_fixture):
        assert test_fixture.get_gpio_ctrl() == 3

    def test_get_power_above_5v_only(self, test_fixture):
        assert test_fixture.get_power_above_5v_only() == 0

    def test_get_req_src_current(self, test_fixture):
        assert test_fixture.get_req_src_current() == 0
