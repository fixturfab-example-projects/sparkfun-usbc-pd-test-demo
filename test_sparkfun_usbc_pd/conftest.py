"""
Test Interface for sparkfun-usbc-pd-test-demo

Create fixtures for the binho that will be used to configure and test the Power
Delivery Board.
"""

import pytest

from binhoHostAdapter import binhoHostAdapter
from binhoHostAdapter import binhoUtilities

from .fixturfab_stusb4500 import STUSB4500


def pytest_addoption(parser):
    parser.addoption(
        "--fixtureID",
        action="store",
        default="0xe4c8b7e850533251382e3120ff09221f",
        help="fixtureID: the GUID of the Binho Nova connected to the test fixture"
    )


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "incremental: Mark test to stop on first failure"
    )


def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._previousfailed = item


def pytest_runtest_setup(item):
    previousfailed = getattr(item.parent, "_previousfailed", None)
    if previousfailed is not None:
        pytest.xfail("previous test failed (%s)" % previousfailed.name)


@pytest.fixture(scope="session")
def test_fixture(request):
    """
    Test Fixture for the sparkfun-usbc-pd-test-demo
    """
    # Get the deviceID of the test device
    binho_nova_device_id = request.config.getoption('--fixtureID')

    # Get the devices COM port
    binho_com_ports = binhoUtilities.getPortByDeviceID(binho_nova_device_id)
    binho_com_port = 0

    if len(binho_com_ports) == 0:
        print("ERROR: No Binho Nova Device found")
        exit(1)
    elif len(binho_com_ports) > 1:
        print("ERROR: More than one Binho Nova Device Found!")
        exit(1)
    else:
        binho_com_port = binho_com_ports[0]
        print(f"Found Binho Nova Device on {binho_com_port}")

    # Connect to the Binho Nova
    print(f"Opening {binho_com_port}")
    host_adapter = binhoHostAdapter.binhoHostAdapter(binho_com_port)
    print("Connecting to Binho Nova host adapter...\n")

    # Configure I2C for communicating with the DUT
    host_adapter.setOperationMode(0, 'I2C')
    host_adapter.setClockI2C(0, 400000)
    host_adapter.setPullUpStateI2C(0, 'EN')

    # Make sure device is set to HEX numerical base
    host_adapter.setNumericalBase('HEX')

    stusb4500 = STUSB4500(host_adapter)

    def teardown():
        host_adapter.close()

    request.addfinalizer(teardown)
    return stusb4500
